
export class Card{

  constructor(
    public id: number,
    public imgUrl: string,
    public name: string,
    public types: Array<any>,
    public abilities: Array<any>,
    public stats: Array<any>,
  ){}
            
             
}

