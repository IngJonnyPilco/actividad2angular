import {  NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router"
import { InicioComponent } from "@modules/home/page/inicio/inicio.component";
import { SessionGuard } from "./core/guards/session.guard";


const appRoute: Routes = [
    
    { path: 'auth', // es para todos los que no esten logueados
        loadChildren: () => import('src/app/modules/auth/auth.module').then(m => m.AuthModule)
    },

    
    { path: '',  // es para todos los que esten logueados
        component: InicioComponent,
        loadChildren: () => import('src/app/modules/home/home.module').then(m => m.HomeModule),
        canActivate: [SessionGuard]
    },
    

];

@NgModule({
    imports: [RouterModule.forRoot(appRoute)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }