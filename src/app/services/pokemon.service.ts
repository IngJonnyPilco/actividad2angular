import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { pokemonsApi , pokemonsApiResult,pokemonsApiResultDetail } from '../interfaces/pokemons';
import { catchError, map, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
 //consumir una api 
  private readonly baseUrl = environment.baseUrl;  
  //inyeccion de dependecias
  //le digo a angular 
  //necesito hacer una instancia
  //de la clase que me permita hacer peticiones
  //http

  //http es una clase con respuesta json
  constructor( private http:HttpClient ) { }

  //esta funcion exporta los datos de la api
  getPokemon(): Observable<pokemonsApiResult[]>{
    //puedo utilizar el metodo get post put delete
    //esto retorna un observable

   return this.http.get<pokemonsApi>(`${this.baseUrl}`)
  .pipe(  map(data => data.results)); 
  }

  
  getPokemonDetails(url:string): Observable<any>{

    return this.http.get<pokemonsApiResultDetail>(url).pipe(
      map(data => {
            return {
              id: data.id,
              name: data.name,
              types: data.types,
              images: data.sprites,
              abilities: data.abilities,
              stats: data.stats,
            }
         }),
      catchError((error)=>{
        const {status, statusText} = error;
        console.log('algo paso revisame...', [status, statusText]);
        return of([]);
      })

    );
  }


  
  
}
