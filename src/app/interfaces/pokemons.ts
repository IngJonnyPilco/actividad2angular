export interface pokemonsApi {
  count: number;
  next: string;
  previous: string;
  results: pokemonsApiResult[];
}



export interface pokemonsApiResult{
  name: string;
  url: string;
}



export interface pokemonsApiResultDetail{
  id: number;
  name: string;
  types:pokemonsApiType[];
  sprites:pokemonsApisprite;
  abilities:pokemonsApiability[];
  stats: pokemonsApiStat[];

}

//types
export interface pokemonsApiType{
  type: pokemonsApiTypeName;
}

export interface pokemonsApiTypeName{
  name: string;
  url: string;
}


// sprites
export interface pokemonsApisprite{
  front_default: string;
  other: pokemonsApispriteother;
}

export interface pokemonsApispriteother{
  dream_world: pokemonsApispriteotherdream_world;
}

export interface pokemonsApispriteotherdream_world{
  front_default: string;
}


// abilities

export interface pokemonsApiability{
  ability: pokemonsApiabilityName;
}

export interface pokemonsApiabilityName{
  name: string;
}

//stats

export interface pokemonsApiStat{
  base_stat: number;
  stat: pokemonsApiStatName;
}

export interface pokemonsApiStatName{
  name: string;
  url: string;
}