import { Injectable } from '@angular/core';
import { MusicModel } from '@models/music.model';
import { Observable, of } from 'rxjs';

import * as dataRaw from '../../../../../src/app/data/music.json';


@Injectable({
  providedIn: 'root'
})
export class TrackService {

  public dataMusicListTrending$: Observable<MusicModel[]> = of([]);
  public dataMusicListRandom$: Observable<MusicModel[]> = of([]);


  constructor() { 

    const  {data}: any = (dataRaw as any).default;
    this.dataMusicListTrending$ = of(data);

    this.dataMusicListRandom$ = new Observable(observer => {
      const trackExplample: MusicModel = {
        _id: '100',
        name: 'Nueva musica',
        album	: 'Ingreso un nuevo almbum', 
        url : 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
        cover: 'https://cdn.pixabay.com/photo/2014/05/21/15/18/musician-349790_960_720.jpg',
      }
      // observer.next([trackExplample]);  //
      setTimeout(() => {
        observer.next([trackExplample]);
      }, 10000);
    })
  }
}
