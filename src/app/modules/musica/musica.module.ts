import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicaRoutingModule } from './musica-routing.module';
import { MusicaPageComponent } from './page/musica-page/musica-page.component';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [
    MusicaPageComponent
  ],
  imports: [
    CommonModule,
    MusicaRoutingModule,
    SharedModule
  ]
})
export class MusicaModule { }
