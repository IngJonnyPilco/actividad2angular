import { Component, OnDestroy, OnInit } from '@angular/core';
import { TrackService } from '@modules/musica/services/track.service';
import { Subscription } from 'rxjs';
import { MusicModel } from 'src/app/models/music.model';
// import * as dataRaw from '../../../../data/music.json';



@Component({
  selector: 'app-musica-page',
  templateUrl: './musica-page.component.html',
  styleUrls: ['./musica-page.component.css']
})
export class MusicaPageComponent implements OnInit,OnDestroy {
  // dataMusicList:Array<MusicModel> = []

  dataMusicListTrending:Array<MusicModel> = []
  dataMusicListRandom:Array<MusicModel> = []

  listSubscriptions$: Array<Subscription> = [];
  
  constructor( private _trackService: TrackService ) { }

  ngOnInit(): void {
  // const  {data}: any = (dataRaw as any).default;
  //   this.dataMusicList = data;
    this.loadDataAll();
    this.loadDataRandom();
  }

  loadDataAll(){
    const observer1$ = this._trackService.dataMusicListTrending$.subscribe(
      (data:Array<MusicModel>) => {

        this.dataMusicListTrending = data;
        this.dataMusicListRandom = data;
      }

    );
    this.listSubscriptions$ = [observer1$];

  } 
  
  loadDataRandom(){
    const observer2$ = this._trackService.dataMusicListRandom$.subscribe(
       (data:Array<MusicModel>) => {
        this.dataMusicListRandom = [...this.dataMusicListRandom, ...data];
      }
 
    )

    this.listSubscriptions$.push(observer2$);
  }



  ngOnDestroy(): void {
    this.listSubscriptions$.forEach(u => u.unsubscribe());
   }
  }
