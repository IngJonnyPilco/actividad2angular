import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicaPageComponent } from './musica-page.component';

describe('MusicaPageComponent', () => {
  let component: MusicaPageComponent;
  let fixture: ComponentFixture<MusicaPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicaPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
