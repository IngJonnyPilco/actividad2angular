import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MusicaPageComponent } from './page/musica-page/musica-page.component';

const routes: Routes = [
  {
    path: '',
    component: MusicaPageComponent,
    outlet: 'child',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicaRoutingModule { }
