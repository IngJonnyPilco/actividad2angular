import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GaleryRoutingModule } from './galery-routing.module';
import { GaleriaComponent } from './page/galeria/galeria.component';
import { SharedModule } from '@shared/shared.module';
import { CardPopUpComponent } from './page/blocks/card-pop-up/card-pop-up.component';
import { CardComponent } from './page/blocks/card/card.component';
import { ListComponent } from './page/blocks/list/list.component';
import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
  declarations: [
    GaleriaComponent,
    CardComponent,
    CardPopUpComponent,
    ListComponent,
  ],
  imports: [
    CommonModule,
    GaleryRoutingModule,
    SharedModule,
    MatDialogModule,
  ]

})
export class GaleryModule { }
