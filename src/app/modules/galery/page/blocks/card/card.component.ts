import { Component, Input, OnInit  } from '@angular/core';
import { Card } from 'src/app/models/card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit  {
  //utilizo la herencia
  @Input() data!: Card  ; // el data!: Card es una convencion que indica que el dato que se le pasa es obligatorio

  constructor() {
  
  }
  
  ngOnInit(): void {

  }

  
  
}
