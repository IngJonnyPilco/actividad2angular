import { ComponentFixture, TestBed } from '@angular/core/testing';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { CardPopUpComponent } from './card-pop-up.component';

describe('CardPopUpComponent', () => {
  let component: CardPopUpComponent;
  let fixture: ComponentFixture<CardPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardPopUpComponent ],
      providers: [{ provide: MatDialogRef, useValue: {} }, 
                  { provide: MAT_DIALOG_DATA, useValue: {}  }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
