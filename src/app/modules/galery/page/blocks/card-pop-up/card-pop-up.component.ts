import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
// import { MatDialogModule} from '@angular/material';
import { Card } from 'src/app/models/card';


@Component({
  selector: 'app-card-pop-up',
  templateUrl: './card-pop-up.component.html',
  styleUrls: ['./card-pop-up.component.css']
})
export class CardPopUpComponent implements OnInit {
  

  card:Card;
  constructor(
      private dialogRef: MatDialogRef<CardPopUpComponent>,
      @Inject(MAT_DIALOG_DATA) data: Card) {
      this.card = data;
  }

  ngOnInit() {

  }

  close() {
      this.dialogRef.close();
  }

  addFavorites(){
    console.log(this.card);
  }
 
}

