import {  Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Card } from 'src/app/models/card';
import { PokemonService } from 'src/app/services/pokemon.service';
import { pokemonsApiability, pokemonsApiResult, pokemonsApiStat, pokemonsApiType } from 'src/app/interfaces/pokemons';


import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { CardPopUpComponent } from '../card-pop-up/card-pop-up.component';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})


export class ListComponent implements OnInit {

  public list: Card [] = [];

  // ListSubscriptions$:Array<Subscription> =[];

  public displayedColumns: string[] = ['id','images', 'name', ];
  dataSource!: MatTableDataSource<Card>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort,{ static: false }) sort!: MatSort ;


  constructor( private _pokemonserviceList: PokemonService, private dialog: MatDialog
    ) { 


    // this.ListSubscriptions$ = [];


    const observer1$ = this._pokemonserviceList.getPokemon().subscribe(
      (pokemons: pokemonsApiResult[]) => {
        pokemons.forEach(
          pokemon => { 
              this._pokemonserviceList.getPokemonDetails(pokemon.url).subscribe(
                (response: any) => {
                    this.list.push( new Card (  
                                                  response.id,
                                                  response.images.other.dream_world.front_default, 
                                                  response.name,  
                                                  response.types.map((type:pokemonsApiType) => type.type.name),
                                                  response.abilities.map((ability:pokemonsApiability) => ability.ability.name), 
                                                  response.stats.map((stat:pokemonsApiStat) => { return { name: stat.stat.name, power: stat.base_stat} })
                                              )  
                                  );
                    this.dataSource = new MatTableDataSource(this.list);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                },
              
              )
            
            } 
        );
      }

    )

    // this.ListSubscriptions$.push(observer1$);
   
  }

  ngOnInit(): void {
    
  }
  

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  // createSubscription() {
  //   this.subscription$ = [
  //     ...this.subscription$,
  //   ];

  // }
  

  
  openDialog(row: Card) {

    const dialogConfig = new MatDialogConfig();

    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;

    dialogConfig.data = row

    this.dialog.open(CardPopUpComponent, dialogConfig);

    // con esto recibo el dato del dialogo https://blog.angular-university.io/angular-material-dialog/

    // const dialogRef = this.dialog.open(CardPopUpComponent, dialogConfig);

    // dialogRef.afterClosed().subscribe(
    //     data => console.log("Dialog output:", data)
    // ); 

  }


  ngOnDestroy(): void {
    // this.ListSubscriptions$.forEach(subscription => subscription.unsubscribe());
    } 
}
