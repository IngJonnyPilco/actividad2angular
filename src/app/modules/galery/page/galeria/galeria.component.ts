import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, Observable,Subscription } from 'rxjs';
import { pokemonsApiResult,pokemonsApiType ,pokemonsApiability,pokemonsApiStat } from 'src/app/interfaces/pokemons';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Card } from 'src/app/models/card';


@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.css'],
})
export class GaleriaComponent implements OnInit, OnDestroy {

  public tituloPagina: string = 'Bienvenido a la Galeria';

  public myFavoritesPokemons: Array<string> = [
    'pikachu',
    'charmander',
    'bulbasaur',
    'dragonite',
    
  ];


  //para crear las cards
  // listSubscriptions$: Array<Subscription>=[];
  public cards: Card [] = [];
  public data: pokemonsApiResult[] = [] ;
  //para crear las lists

  //
  constructor(
    private _pokemonservice: PokemonService,
    private _pokemonserviceDetails: PokemonService,
  ) {

    // this.listSubscriptions$ =[];


    // obtiene datos de la api y los filtra segun mis pokemons favoritos
    const observerPokemon1$ = this._pokemonservice
    .getPokemon()
    .pipe(
      map((response: pokemonsApiResult[]) =>
        response.filter((pokemon) =>
          this.myFavoritesPokemons.includes(pokemon.name)
        )
      )
    ).subscribe(
      (pokemons: pokemonsApiResult[]) => {

        this.data = pokemons;
        this.data.forEach(pokemonDetail => {
          this._pokemonserviceDetails.getPokemonDetails(pokemonDetail.url).subscribe(
            (response: any) => {
              this.cards.push( new Card (   
                                            response.id,
                                            response.images.other.dream_world.front_default, 
                                            response.name,  
                                            response.types.map((type:pokemonsApiType) => type.type.name),
                                            response.abilities.map((ability:pokemonsApiability) => ability.ability.name), 
                                            response.stats.map((stat:pokemonsApiStat) => { return { name: stat.stat.name, power: stat.base_stat} }) 
                                        )
                            );
              }
            )

        });
      },
    );

    // obtiene los datos de la api 

  //  this.listSubscriptions$.push(observerPokemon1$);

  }


  ngOnInit(): void {

  }

  // createSubscription() {
  //   this.subscription$ = [
  //     ...this.subscription$,
  //   ];
  
  // }
  
  ngOnDestroy(): void {
    // this.listSubscriptions$.forEach(subscription => subscription.unsubscribe());
    } 


}
