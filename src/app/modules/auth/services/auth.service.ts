import { Injectable } from '@angular/core';
import { map, Observable, of } from 'rxjs';
import * as dataRaw from '../../../../app/dataAuth/user.json';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public dataUser$: Observable<any> = of([]);

  constructor() {
    const  {data}: any = (dataRaw as any).default;
    this.dataUser$ = of(data); //creo el observable
   }

  public sendCredentials(email:string, password:string):Observable<any>{
    //en lugar de esto se debe usar una api y verificar el body de la respuesta
    return this.dataUser$.pipe(
      map((data:any):any =>{

        //si data[0].email === email && data[0].password === password devolver los datos caso contrario devolver un error
        if(data[0].email === email && data[0].password === password){
          return data;
        }
        else{
          throw new Error('datos incorrectos');
        }

      
      })
      );
    // con la api seria de la siguiente manera 
    // const body = {
    //   email,
    //   password
    // }
    // return this.http.post('http://localhost:3000/api/login',body)
  }
}
