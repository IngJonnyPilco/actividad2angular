import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@modules/auth/services/auth.service';
import { CookieService } from 'ngx-cookie-service';


/** Error when invalid control is dirty, touched, or submitted. */


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  errorSession = false;
  formLogin : FormGroup = new FormGroup({});
  
  constructor( private authService: AuthService , 
               private cockie: CookieService,
               private router: Router) { }

  hide = true;

  
  ngOnInit(): void {
    this.formLogin = new FormGroup(
      {
        email: new FormControl('',[
          Validators.required,
          Validators.email,
          
        ]),
        password: new FormControl('',[
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(20)
        ]),
      
      }
    );
    
    //poner
    //agregar a errorEmail los errores del formLogin.email

  }
  

  sendLogin(){
    const {email,password}= this.formLogin.value;
    this.authService.sendCredentials(email,password).subscribe(
      {  next: (responseOk:any)=>{
        // console.log('datos correctos');
        // this.cockie.set('token');
          const tokenSession = responseOk[1].tokenSession;      
          this.cockie.set('token',tokenSession,4,'/');        
          this.router.navigate(['/']);  
         },
        error:(error) =>{
        this.errorSession = true
        // console.log('ocurrio un error con tus credenciales ',error);
         }
      }
    )
  }

}
