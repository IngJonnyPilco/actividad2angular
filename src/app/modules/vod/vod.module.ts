import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VodRoutingModule } from './vod-routing.module';
import { VodPageComponent } from './page/vod-page/vod-page.component';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [
    VodPageComponent,
    
  ],
  imports: [
    CommonModule,
    VodRoutingModule,
    SharedModule,

  ]
})
export class VodModule { }
