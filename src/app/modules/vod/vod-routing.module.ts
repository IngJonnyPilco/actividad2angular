import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VodPageComponent } from './page/vod-page/vod-page.component';

const routes: Routes = [
  {
    path: '',
    component: VodPageComponent,
    outlet: 'child'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VodRoutingModule { }
