/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { VodPageComponent } from './vod-page.component';

describe('VodPageComponent', () => {
  let component: VodPageComponent;
  let fixture: ComponentFixture<VodPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VodPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VodPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
