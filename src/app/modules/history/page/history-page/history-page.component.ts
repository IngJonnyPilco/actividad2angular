import { Component, OnInit } from '@angular/core';
import { MusicModel } from '@models/music.model';
import { SearchService } from '@modules/history/services/search.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.css']
})
export class HistoryPageComponent implements OnInit {

  listResults$: Observable<any> = of([]);

  constructor( private searchServ: SearchService) { }

  ngOnInit(): void {
  }


  recibirData(event:string):void{
    // console.log('recibiendo en el padre: ',event); //recibo el dato del hijo
    this.listResults$= this.searchServ.searchTracks(event); // y lo envio a hijo  el cual extrae los datos de la api segun el valor que enviemos
  }

  //la pipeta async resuelve el observable  con el ultimo valor y del resto se desuscribe automaticamente
}
