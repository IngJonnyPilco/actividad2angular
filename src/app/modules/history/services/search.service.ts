import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import * as dataRaw from 'src/app/data/music.json';


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  

  constructor() { }

  searchTracks(value:string):Observable<any> {
    const {data} = (dataRaw as any).default;

    const result = data.filter((item:any) => item.name.toLowerCase().includes(value.toLowerCase()));

    return of(result);
    
  }
}
