import { EventEmitter,Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Output() callBackData:EventEmitter<any> = new EventEmitter();  //envio el dato a el componente padre (callBackData) se envia en <app-search (callCBackData)>

  value:string = '';

  constructor() { }

  ngOnInit(): void {
  }

  callSearch():void{
    if(this.value.length > 3){
      this.callBackData.emit(this.value);   //emito el dato callBackData al componente padre
      // console.log('emitiendo desde el hijo:',this.value);
    }
  }
}
