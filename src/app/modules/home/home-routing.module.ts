import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'buscar',
    loadChildren: () => import('@modules/history/history.module').then(m => m.HistoryModule)
  },
  {
    path: 'galeria',
    loadChildren: () => import('@modules/galery/galery.module').then(m => m.GaleryModule)
  },
  { path: 'favoritos',
    loadChildren: () => import('@modules/favoritos/favoritos.module').then(m => m.FavoritosModule)
  },
  { path: 'vod', 
    loadChildren: () => import('src/app/modules/vod/vod.module').then(m => m.VodModule),
  },

  { path: 'musica', 
    loadChildren: () => import('src/app/modules/musica/musica.module').then(m => m.MusicaModule),
  },
  {
    path:'**',
    redirectTo: 'musica'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
