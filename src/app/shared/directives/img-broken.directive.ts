import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'img[appImgBroken]'
})
export class ImgBrokenDirective {

  @Input() customImg :string ='';

  //el evento error es lansado sobre un elemento objec cuando el recurso no se encuentra  o no es accesible
  @HostListener('error') handleError() :void{
    const elementNative = this.host.nativeElement;
    // elementNative.src = '../../../assets/img/pikachu.png';
    elementNative.src = this.customImg;
    // console.log('no hay imagen ', this.host);
  }          // va a escuchar el host en este caso el img

  //todo: elementref es para hacer referencia a un elemento
  constructor(private host: ElementRef ) {

   }
}
