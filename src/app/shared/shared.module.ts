import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { FooterComponent } from './components/footer/footer.component';
import { MediaplayerComponent } from './components/mediaplayer/mediaplayer.component';
import { MaterialModule } from 'src/shared/material.module';
import { CardPlayerComponent } from './components/card-player/card-player.component';
import { SectionGenericComponent } from './components/section-generic/section-generic.component';
import { PlayListHeaderComponent } from './components/play-list-header/play-list-header.component';
import { PlayListBodyComponent } from './components/play-list-body/play-list-body.component';
import { RouterModule } from '@angular/router';
import { OrderListPipe } from './pipe/order-list.pipe';
import { ImgBrokenDirective } from './directives/img-broken.directive';

import { CarouselModule } from 'ngx-owl-carousel-o';
import { VjsPlayerComponent } from './components/vjs-player/vjs-player.component';


import { VgCoreModule } from '@videogular/ngx-videogular/core';
import { VgControlsModule } from '@videogular/ngx-videogular/controls';
import { VgOverlayPlayModule } from '@videogular/ngx-videogular/overlay-play';
import { VgBufferingModule } from '@videogular/ngx-videogular/buffering';


@NgModule({ 
  declarations: [ //todo: declara de componentes, directivas y pipes
    NavComponent,
    FooterComponent,
    MediaplayerComponent,
    CardPlayerComponent,
    SectionGenericComponent,
    PlayListHeaderComponent,
    PlayListBodyComponent,
    OrderListPipe,
    ImgBrokenDirective,
    VjsPlayerComponent,
    
  ],
  imports: [   //todo: recibe los modolulos a utilizar en su area
    CommonModule,
    MaterialModule,
    RouterModule,
    CarouselModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
  ],
  exports: [   //todo: componentes, directivas y pipes   ... Los envia a todos los modulos que lo requieran
    MaterialModule,
    NavComponent,
    FooterComponent,
    MediaplayerComponent,
    CardPlayerComponent,
    SectionGenericComponent,
    PlayListHeaderComponent,
    PlayListBodyComponent,
    OrderListPipe,
    ImgBrokenDirective,
    VjsPlayerComponent,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
  ]
})
export class SharedModule { }
