import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PlayerMediaService } from '@shared/services/player-media.service';
import { Subscription } from 'rxjs'; 
import { MusicModel } from 'src/app/models/music.model';

@Component({
  selector: 'app-mediaplayer',
  templateUrl: './mediaplayer.component.html',
  styleUrls: ['./mediaplayer.component.css']
})
export class MediaplayerComponent implements OnInit, OnDestroy {
  // el #progressBar es un alias de la etiqueta html que nos permiete manipular el DOM  para ello necesitamos el viewChild
  @ViewChild('progressBar') progressBar:ElementRef = new ElementRef('');

  state : string = 'play_arrow';

  mockCover?: MusicModel;
  

  listObservers$: Array<Subscription> = [];

  constructor( public multimediaService: PlayerMediaService ) { }

  ngOnInit(): void {

    // const observer1: Subscription = this.multimediaService.callback.subscribe(
    //   (track: MusicModel) => {
    //     console.log('recibiendo cancion',track);
    //   }
    // );

    // this.listObservers$.push(observer1);    

    const observer1 =this.multimediaService.playedStatus$.subscribe(
      (status: string) => {
        this.state = status;
      }
    );
    this.listObservers$.push(observer1);
  }

  ngOnDestroy(): void {
    this.listObservers$.forEach(obs => obs.unsubscribe());
  }

  handlePosition(event: MouseEvent): void {
    const {clientX} = event;
    const elementNative = this.progressBar.nativeElement;
    const {x, width} = elementNative.getBoundingClientRect();
    const clickX = clientX - x;

    const percentFromX = (clickX * 100) / width;
    
    this.multimediaService.seekAudio(percentFromX);
  }
}
