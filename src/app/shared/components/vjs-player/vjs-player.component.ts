import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-vjs-player',
  templateUrl: './vjs-player.component.html',
  styleUrls: ['./vjs-player.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class VjsPlayerComponent implements OnInit {

  videoItems = [
    {
      name: 'Video one',
      src: 'http://static.videogular.com/assets/videos/videogular.mp4',
      poster: 'https://cdn.pixabay.com/photo/2016/10/20/18/35/earth-1756274__480.jpg',
      autor: 'Autor 1',
      views: '1.2M',
      type: 'video/mp4'
    },
    {
      name: 'Video two',
      src: 'http://static.videogular.com/assets/videos/big_buck_bunny_720p_h264.mov',
      poster: 'https://cdn.pixabay.com/photo/2020/02/29/18/59/rabbit-4890861__340.jpg',
      autor: 'Autor 2',
      views: '12.343',
      type: 'video/mp4'
    },
    {
      name: 'Video three',
      src: 'http://static.videogular.com/assets/videos/elephants-dream.mp4',
      poster: 'https://cdn.pixabay.com/photo/2016/07/28/19/38/lost-places-1549096__480.jpg',
      autor: 'Autor 3',
      views: '3.413M',
      type: 'video/mp4'
    },
    {
      name: 'Video four',
      src: 'http://static.videogular.com/assets/videos/elephants-dream.mp4',
      poster: 'https://cdn.pixabay.com/photo/2016/07/11/20/34/lost-places-1510592__340.jpg',
      autor: 'Autor 4',
      views: '7.3M',
      type: 'video/mp4'
    },
    {
      name: 'Video five',
      src: 'http://static.videogular.com/assets/videos/elephants-dream.mp4',
      poster: 'https://cdn.pixabay.com/photo/2018/09/05/04/21/flowers-3655451__340.jpg',
      autor: 'Autor 5',
      views: '2.413M',
      type: 'video/mp4'
    },
    {
      name: 'Video six',
      src: 'http://static.videogular.com/assets/videos/elephants-dream.mp4',
      poster: 'https://cdn.pixabay.com/photo/2016/12/03/17/38/building-1880261__340.jpg',
      autor: 'Autor 6',
      views: '3.452',
      type: 'video/mp4'
    },
    {
      name: 'Video seven',
      src: 'http://static.videogular.com/assets/videos/elephants-dream.mp4',
      poster: 'https://cdn.pixabay.com/photo/2016/09/11/16/47/car-1661767__340.jpg',
      autor: 'Autor 7',
      views: '3.413',
      type: 'video/mp4'
    },
    {
      name: 'Video eigth',
      src: 'http://static.videogular.com/assets/videos/elephants-dream.mp4',
      poster: 'https://cdn.pixabay.com/photo/2016/11/02/14/18/owl-1791700__340.png',
      autor: 'Autor8',
      views: '1M',
      type: 'video/mp4'
    }
  ];

  activeIndex = 0;
  currentVideo = this.videoItems[this.activeIndex];
  data: any;

  videoPlayerInit(data: any) {
    this.data = data;
    this.data.getDefaultMedia().subscriptions.loadedMetadata.subscribe(this.initVdo.bind(this));
    this.data.getDefaultMedia().subscriptions.ended.subscribe(this.nextVideo.bind(this));
  }
  nextVideo() {
    this.activeIndex++;
    if (this.activeIndex === this.videoItems.length) {
      this.activeIndex = 0;
    }
    this.currentVideo = this.videoItems[this.activeIndex];
  }
  initVdo() {
    this.data.play();
  }
  startPlaylistVdo(item: any, index: number) {
    this.activeIndex = index;
    this.currentVideo = item;
  }

    constructor(  ) { }


  
    ngOnInit() {

    }
  
    ngOnDestroy() {
      // destroy player
    }
 
}
