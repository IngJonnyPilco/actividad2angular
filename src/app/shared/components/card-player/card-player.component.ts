import { Component, Input, OnInit } from '@angular/core';
import { PlayerMediaService } from '@shared/services/player-media.service';
import { MusicModel } from 'src/app/models/music.model';

@Component({
  selector: 'app-card-player',
  templateUrl: './card-player.component.html',
  styleUrls: ['./card-player.component.css']
})
export class CardPlayerComponent implements OnInit {
  @Input() mode: 'carrousel' | 'cards' = 'cards';
  @Input() track: MusicModel = {
    _id: '',
    name: '',
    album: '',
    url: '',
    cover: '',
  };
  constructor(private multimediaService : PlayerMediaService ) { }

  ngOnInit(): void {
  }

  sendPlay(track :MusicModel):void{
    // console.log('enviando cancion al reproductor', track);
    // this.multimediaService.callback.emit(track); // suscribe para escuchar y emit para enviar

    this.multimediaService.musicInfo$.next(track);  //es mejor hacerlo con behaviorSubject
  }

}
