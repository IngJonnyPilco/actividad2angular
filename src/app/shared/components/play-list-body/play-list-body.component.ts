import { Component, Input, OnInit } from '@angular/core';
import * as dataRaw from 'src/app/data/music.json';
import { MusicModel } from 'src/app/models/music.model';

@Component({
  selector: 'app-play-list-body',
  templateUrl: './play-list-body.component.html',
  styleUrls: ['./play-list-body.component.css']
})
export class PlayListBodyComponent implements OnInit {
  @Input() canciones: MusicModel[] = [];
  optionSort: {property: string | null,  order: string} ={property: null, order: 'desc'};
  constructor() { }

  ngOnInit(): void {
    const {data} = (dataRaw as any).default;
    this.canciones = data;
  }

  changeSort(property: string) :void {
    const {order} = this.optionSort;
    this.optionSort = {
      property: property, 
      order: order === 'desc' ? 'asc' : 'desc'
    };
  }

}
