import { Component, Input, OnInit } from '@angular/core';
import { MusicModel } from 'src/app/models/music.model';

import { OwlOptions } from 'ngx-owl-carousel-o';


@Component({
  selector: 'app-section-generic',
  templateUrl: './section-generic.component.html',
  styleUrls: ['./section-generic.component.css']
})
export class SectionGenericComponent implements OnInit {
  @Input() title: string = '';
  @Input() mode: 'carrousel' | 'cards' = 'carrousel';

  @Input() dataMusic: Array<MusicModel> = [];
  
  constructor() { }

  ngOnInit(): void {
  }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    navText: [' ', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: false
  }
}
