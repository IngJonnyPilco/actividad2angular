import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
// import { Observable } from 'rxjs';
// import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  // showFiller = false;
  // isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  //   .pipe(
  //     map(result => result.matches),
  //     shareReplay()
  //   );

  mainMenu :{
    defaultOptions: Array<any>
  } = {
    defaultOptions: []
  }

  constructor(
    // private breakpointObserver: BreakpointObserver
    private router: Router
    ) {}

  ngOnInit(){
    this.mainMenu.defaultOptions = [
      {
        name: 'Inicio',
        icon: 'home',
        router: ['/'],
        // query: {
        //   key1: 'hola mundo',
        // }
      },
      {
        name: 'Buscar',
        icon: 'search',
        router: ['/', 'buscar'],
        // query: {
        //   hola: 'mundo',
        // }
      },
      {
        name: 'Galeria',
        icon: 'collections',
        router: ['/', 'galeria'],
        // query: {
        //   hola: 'mundo',
        // }
      },
      {
        name: 'Videos',
        icon: 'ondemand_video',
        router: ['/','vod'],
        // query: {
        //   hola: 'mundo',
        // }
   
      },
      {
        name: 'Musica',
        icon: 'music_note',
        router: ['/','musica'],
        // query: {
        //   hola: 'mundo',
        // }
 
      },
      {
        name: 'Favoritos',
        icon: 'favorite',
        router: ['/', 'favoritos'],
        // query: {
        //   hola: 'mundo',
        // }

      },

    ]
  }

  goTo($event:any): void {
    // this.router.navigate(['/','favoritos'],{
    //   queryParams: {
    //     key1: 'value1',
    //     key2: 'value2',
    //     key3: 'value3',
    //   }
    // }); //todo: uso del queryParams
  }
}

