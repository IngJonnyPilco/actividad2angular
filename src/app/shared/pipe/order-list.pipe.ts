import { Pipe, PipeTransform } from '@angular/core';
import { MusicModel } from '@models/music.model';

@Pipe({
  name: 'orderList'
})
export class OrderListPipe implements PipeTransform {




  transform(value: Array<any>, args: string | null = null, sort: string = 'desc'): MusicModel[] {

    if (args === null) {
      return value
    } else {

      const tmpList = value.sort((a, b) => {
        if (sort === 'desc') {
          return a[args] > b[args] ? -1 : 1;
        } else {
          return a[args] < b[args] ?  1 : -1;
        }
      });

      return (sort === 'desc') ? tmpList : tmpList.reverse()
    }

  }
 
  

}
