import { TestBed } from '@angular/core/testing';

import { PlayerMediaService } from './player-media.service';

describe('PlayerMediaService', () => {
  let service: PlayerMediaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlayerMediaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
