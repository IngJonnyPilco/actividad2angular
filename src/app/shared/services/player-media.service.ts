import { EventEmitter, Injectable } from '@angular/core';
import { MusicModel } from '@models/music.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerMediaService {

  callback: EventEmitter<any> = new EventEmitter(); // EMISON DE ENVENTOS ESTE SERVICIO ES EL QUE SE
                                                    // ENCARGA DE ACTUALIZAR EL PLAYER
  
  public musicInfo$: BehaviorSubject<any> = new BehaviorSubject<any>(undefined);
  public audio!: HTMLAudioElement;
  
  public timeElapsed$: BehaviorSubject<string> = new BehaviorSubject<string>('00:00');
  public timeRemain$: BehaviorSubject<string> = new BehaviorSubject<string>('-00:00');

  public playedStatus$: BehaviorSubject<string> = new BehaviorSubject<string>('paused');

  public playedPercentage$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  // public mockDefaultMusic ={
  //   name: 'hola',
  //   album: 'hola',
  //   cover: 'https://cdns-images.dzcdn.net/images/cover/1db3f8f185e68f26feaf0b9d72ff1645/350x350.jpg',
  //   url: 'https://mus3.about-in.tk/skm/b3091ee2-eb8f-46ce-87b8-7d3adca28347.mp3',
  //   _id: 1
  // };

  constructor() { 
    this.audio = new Audio();
    this.musicInfo$.subscribe(
      (audio) => {
        this.setAudio(audio);
      }
    );

    this.listenAllEvents();
  }
  //funciones privadas
  private listenAllEvents(): void {
    this.audio.addEventListener('timeupdate',this.calculateTime,false );
    this.audio.addEventListener('playing',this.setPlayerStatus,false );
    this.audio.addEventListener('play',this.setPlayerStatus,false );
    this.audio.addEventListener('pause',this.setPlayerStatus,false );
    this.audio.addEventListener('ended',this.setPlayerStatus,false );

  }
  //todo: para los tiempos de reproduccion
    private calculateTime = ()=> {

      const {duration, currentTime} = this.audio;
      // console.log(this.audio);
      // console.log([duration, currentTime]);
      this.setTimeLapsed(currentTime);
      this.setTimeRemain(currentTime, duration);
      this.setPlayedPercentage(currentTime, duration);
    }

    private setTimeLapsed(currentTime:number): void {
      let seconds = Math.floor(currentTime % 60);
      let minutes = Math.floor((currentTime / 60) % 60);

      const displaySeconds = (seconds < 10) ? `0${seconds}` : seconds;
      const displayMinutes = (minutes < 10) ? `0${minutes}` : minutes;
      const displayFormat = `${displayMinutes}:${displaySeconds}`;
      this.timeElapsed$.next(displayFormat);
    }

    private setTimeRemain(currentTime:number, duration:number): void {
      let timeLeft = duration - currentTime;
      let seconds = Math.floor(timeLeft % 60);
      let minutes = Math.floor((timeLeft / 60) % 60);
      const displaySeconds = (seconds < 10) ? `0${seconds}` : seconds;
      const displayMinutes = (minutes < 10) ? `0${minutes}` : minutes;
      const displayFormatRemain = `-${displayMinutes}:${displaySeconds}`; //tiempo restante

      this.timeRemain$.next(displayFormatRemain);
    }

  //todo: para play

  private setPlayerStatus = (state:any)=> {
    //captara los eventos pero en la funcion publica cambia los valores togglePlayer
    switch (state.type) {
      default: this.playedStatus$.next('paused'); break;
      case 'play': this.playedStatus$.next('play'); break;
      case 'playing': this.playedStatus$.next('playing'); break;
      case 'pause': this.playedStatus$.next('pause'); break;
      case 'ended': this.playedStatus$.next('ended'); break;
    }
  }

    //todo: para la barra de progreso
  
  public setPlayedPercentage(currentTime:number, duration:number): void {
    let percentage = (currentTime * 100) / duration;
    this.playedPercentage$.next(percentage);
  }

  //funciones publicas
  public setAudio(musica:MusicModel): void {
    // console.log(musica.cover);
    try {
      this.audio.src = musica.url;
      this.audio.play();
      
    } catch (error) {
    }
      
  }

  //todo: para play y pause
  public togglePlayer(): void {
    //con estos metodos propios podemos controlar el audio
    (this.audio.paused) ? this.audio.play() : this.audio.pause();
  }

  //todo: para manejar la barra

  public seekAudio(percentage:number): void {
    const {duration} = this.audio;
    const percentageToSecond = (percentage * duration) / 100;
    console.log(percentageToSecond);
    
    //todo: para que se mueva la barra
    this.audio.currentTime = percentageToSecond;
  }
}
